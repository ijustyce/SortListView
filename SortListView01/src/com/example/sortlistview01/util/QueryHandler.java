package com.example.sortlistview01.util;

import java.util.List;

import com.example.sortlistview01.activity.MainActivity.QueryListener;
import com.example.sortlistview01.bean.Contact;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;

public class QueryHandler extends AsyncQueryHandler{
	private ContentResolver resolver;
	private QueryListener listener;
	
	public QueryHandler(ContentResolver cr , QueryListener listener) {
		super(cr);
		this.resolver = cr;
		this.listener = listener;
	}

	@Override
	protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
		List<Contact> list = QueryContactUtil.queryContact(resolver, cursor);
		listener.loadComplete(list);
		super.onQueryComplete(token, cookie, cursor); 
	}
	

}
