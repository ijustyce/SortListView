package com.example.sortlistview01.bean;

public class Contact {

	/**
	 * 联系人id
	 */
	private String id;

	/**
	 * 联系人姓名
	 */
	private String name;

	/**
	 * 联系人电话
	 */
	private String telphone;

	/**
	 * 排序字母
	 */
	private String sortKey;

	public Contact() {
		super();
	}

	public Contact(String id, String name, String telphone) {
		super();
		this.id = id;
		this.name = name;
		this.telphone = telphone;
	}

	public Contact(String id, String name, String telphone, String sortKey) {
		super();
		this.id = id;
		this.name = name;
		this.telphone = telphone;
		this.sortKey = sortKey;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", name=" + name + ", telphone="
				+ telphone + ", sortKey=" + sortKey + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

}