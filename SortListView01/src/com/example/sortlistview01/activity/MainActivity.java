package com.example.sortlistview01.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.example.sortlistview01.bean.Contact;
import com.example.sortlistview01.util.QueryHandler;
import com.example.sortlistview01.util.ViewHolder;
import com.example.sortlistview01.view.SideBarView;
import com.example.sortlistview01.view.SideBarView.OnTouchSlideBarListener;

public class MainActivity extends Activity {
	private QueryHandler queryHandler;
	private QueryListener listener;
	private ListView listView;
	private MyAdapter adapter;
	private List<Contact> list = new ArrayList<Contact>();
	private final int LOAD_SUC = 1;
	
	private TextView textViewDialog;
	private SideBarView sideBarView;
	
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(Message msg) {
			if (msg.what == LOAD_SUC) {
				List<Contact> newList = (List<Contact>) msg.obj;
				if (newList != null && newList.size() > 0) {
					addIntoList(newList);
				}
			}
		}
	};
	
	private void addIntoList(List<Contact> newList){
		for(Contact contact : newList){
			list.add(contact);
		}
		adapter.notifyDataSetChanged();
	}

	public interface QueryListener {
		void loadComplete(List<Contact> list);
	}

	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(MainActivity.this).inflate(
						R.layout.layout_list_item, null);
			}
			TextView tv_name = ViewHolder.get(convertView, R.id.tv_name);
			TextView tv_telphone = ViewHolder
					.get(convertView, R.id.tv_telphone);
			tv_name.setText(list.get(position).getName());
			tv_telphone.setText(list.get(position).getTelphone());
			return convertView;
		}

	}

	private void initView() {
		listView = (ListView) findViewById(R.id.listview);
		adapter = new MyAdapter();
		listView.setAdapter(adapter);
		textViewDialog = (TextView) findViewById(R.id.dialog);
		sideBarView = (SideBarView) findViewById(R.id.slideBar);
		sideBarView.setTextView(textViewDialog);
		sideBarView.setOnTouchSlideBarListener(new OnTouchSlideBarListener() {
			@Override
			public void touchedIndex(int index) {
				listView.setSelection(index);
			}
		});
	}

	private void initData() {
		queryHandler = new QueryHandler(getContentResolver(),
				new QueryListener() {
					@Override
					public void loadComplete(List<Contact> list) {
						handler.obtainMessage(LOAD_SUC, list).sendToTarget();
					}
				});
		queryHandler.startQuery(0, null, ContactsContract.Contacts.CONTENT_URI,
				null, null, null, "display_name COLLATE LOCALIZED");
	}

	private void init() {
		initView();
		initData();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
	}

}
